"""
Generate a file containing lines like the following:
0 10 P
1 8 K
1 10 :
1 9 S
0 10 #

First value is 0 or 1, second is random int between 8 and 20, last value is 'A'-'Z' or ':' or '#'
"""
import random
import sys
import logging


def get_random_fence_pattern():
    return random.choice(''.join([chr(i) for i in range(ord('A'), ord('Z') + 1)]) + ':#')


def get_random_fence_length():
    return random.randrange(8, 20 + 1)


def get_random_parity():
    return random.randint(0, 1)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    print(sys.argv)
    if len(sys.argv) > 0:
        max_street_length = int(sys.argv[1])
    else:
        max_street_length = 1000

    logging.debug("Max street length is: {}".format(max_street_length))
    odd_length = 0
    even_length = 0
    with open("./kerites.txt", "w+") as output_file:
        while max(odd_length, even_length) < max_street_length:
            parity, fence_length, fence_pattern = get_random_parity(), get_random_fence_length(), get_random_fence_pattern()

            if parity % 2 == 0:
                if max_street_length < even_length + fence_length:
                    logging.debug(
                        "Breaking out, even_length ({}) reached the max! ({})".format(even_length, max_street_length))
                    break
                else:
                    even_length += fence_length
            else:
                if max_street_length < odd_length + fence_length:
                    print("Breaking out, odd_length ({}) reached the max! ({})".format(odd_length, max_street_length))
                    break
                else:
                    odd_length += fence_length
            logging.debug("Added line: '{} {} {}'".format(parity, fence_length, fence_pattern))
            logging.debug("Current longest fence length: {}".format(max(even_length, odd_length)))
            output_file.write("{} {} {}\n".format(parity, fence_length, fence_pattern))
