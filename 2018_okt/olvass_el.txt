A feladat file-jának generálásához a következő parancsot kell beadni:
python kerites_txt_generator.py
Ha meg szeretnénk adni a maximális kerítéshosszt az alapértelmezett 1000 hosszúságú helyett (pl. 200-at):
python kerites_txt_generator.py 200